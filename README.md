# Python script to verify batch of Bitcoin signatures


### Requirements

Python3 - the script has only been lightly tested on some systems with
Python 3.

If you want to run with `-ofek_ecdsa` option, you need to install 
the 'ecdsa' module using pip3 or however you usually install Python 3
modules. It may be called 'python-ecdsa' or similar on your system.
This module is the only one that has not been bundled, as it needs to pull
in several dependencies. Your package manager should hopefully deal with
those.


### Caveats / limitations

Signatures are expected to be against bitcoind/Electrum formatted messages
where the actual message text is preceded by a "\x18Bitcoin Signed Message:\n"
and followed by a varint of the message length, then the actual message
bytes.

There is a regression currently:
When using '-ofek_ecdsa' option, currently the pubkeys.txt may not contain
Bitcoin P2PKH addresses - it must contain uncompressed public keys.
Reconstructing the public key from the message and signatures using
the bit/ecdsa/coincurve libraries is something that still needs to be
resolved.


### Usage:

Unpack or clone this repository to some directory.

Create the required input text files (see next section on data format).

Run the script:

    $ python3 verify_sigs.py

The output will tell you at the end whether all signatures verified
successfully, and if not, how many and which rows failed signature verification.

Running with Lev / Warner crypto libraries:

    $ python3 verify_sigs.py -ofek_ecdsa

In this mode, the script loads methods from Ofek Lev's 'bit' library (bundled)
and Brian Warner's 'ecdsa' library (not bundled).

The resulting output should always be the same as running without this
option, but if you notice any discrepancy, please raise a bugtracker Issue.


### Input files

(This is also described in the header of the verify_sigs.py script)

There are three input data files which are expected in the working directory:

* pubkeys.txt
* messages.txt
* signatures.txt

Each file must be a plain text file, with one data item per line.

The pubkeys.txt file must contain Bitcoin public keys or addresses.
Public keys must be in uncompressed format.  Bitcoin addresses are the
usual (P2PKH) format.
Public keys and addresses can be mixed in the same file.

The messages.txt file must contain one text message per line.
Multi-line messages are not handled by the input data format.

The signatures.txt file must contain one Bitcoin 'bare' signature per line.

### The provided example files

* messages.txt - just a file with two messages
* pubkeys.txt - a copy of pubkeys.txt.addr
* pubkeys.txt.addr - a pubkeys file containing Bitcoin addresses
* pubkeys.txt.uncompressed - pubkeys in uncompressed format corresponding to same addresses
* signatures.txt - a copy of signatures.txt.bad, with bad sig on row 2
* signatures.txt.bad - explained above
* signatures.txt.good - signatures all good in this one


### Batteries included

A required module ('bitcoin') from Vitalik Buterin's pybitcointools is bundled
together with this distribution so you don't need to obtain it separately.
This library is used by default, but there is an option to use a different
set of libraries which are better maintained and may be safer to use.

The bitcoin/ and pybitcointools/ folders are from Buterin's library, with
a minor fix made to bitcoin/main.py to get the script to handle verification
against a Bitcoin address.

The ofek-bit/ and bit/ folders are from Ofek Lev's 'bit' library, and are
also under MIT (see LICENSE.txt files in those folders).

Some convenient code for double SHA256 and for padding a Bitcoin message
in prepapartion for signing have been borrowed from StackOverflow user
'Alai'. You can find them in the separate 'alai.py' module along with
a link to their StackOverflow origin.


### Licenses:

The batch verification script is made available under the MIT license.
See the top level LICENSE file.

Vitalik's modules have their own LICENSE files - see subfolders. They are
dual-licensed (public domain or MIT, whichever fits your jurisdiction).

Lev's modules have their own LICENSE.txt files - see subfolders. They are
licensed under MIT.

The 'ecdsa' module is in the public domain (MIT?) but has not been bundled
as it needs further dependencies of its own (coincurve, maybe more) which
are better handled by a package manager.
