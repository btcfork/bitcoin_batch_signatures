# DoubleSha256 class and pad_message functions borrowed from:
# https://stackoverflow.com/questions/59689746/how-to-verify-a-signature-made-by-trezor-wallet 

import hashlib

class DoubleSha256:
   def __init__(self, *args, **kwargs):
      self._m = hashlib.sha256(*args, **kwargs)

   def __getattr__(self, attr):
      if attr == 'digest':
         return self.double_digest
      return getattr(self._m, attr)

   def double_digest(self):
      m = hashlib.sha256()
      m.update(self._m.digest())
      return m.digest()

def pad_message(message):
   return "\x18Bitcoin Signed Message:\n".encode('UTF-8') + bytes([len(message)]) + message.encode('UTF-8')
