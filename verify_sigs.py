#!/usr/bin/env python3

# A script to batch-verify a bunch of Bitcoin signatures against a list
# of public keys (or addresses) & a list of (one line) text messages.
#
# Inputs required (all files are PLAIN TEXT, ONE ITEM PER LINE):
#
#   pubkeys.txt    : the public keys (uncompressed) or addresses
#   messages.txt   : the messages (handles only single-line messages)
#   signatures.txt : bare signatures (as produced by common clients)
#
# The items in each file must correspond positionally to each other, i.e.
# signature on line N is verified against pubkey N and message N.
#
# NOTE: any newlines are stripped from messages before verification...
#
# The script takes one optional command line argument, which switches
# the verification libraries used. If the '-ofek_ecdsa' option is not
# passed, it uses the bundled 'pybitcointools' module, otherwise it
# expects to find the 'ecdsa' module and also uses some methods from
# the bundled 'bit' library by Ofek Lev.

import sys


def show_usage():
   print("Bitcoin signature batch verification tool\n"
         "\n"
         "Usage: verify_sigs.py [-ofek_ecdsa]\n"
         "\n"
         "  The '-ofek_ecdsa' option disables use of pybitcointools and\n"
         "  instead uses a combination of Ofek Lev's 'bit' library and\n"
         "  Brian Warner's python-ecdsa ('ecdsa') library.\n"
         "  The default is to use the bundled pybitcointools, but we\n"
         "  recommend that you compare the outputs of both modes.\n"
         "\n"
         "The tool expects three plain text files in the working directory:\n"
         "\n"
         "  pubkeys.txt    - one uncompressed pubkey or address per line\n"
         "  messages.txt   - one message per line\n"
         "  signatures.txt - one Bitcoin bare signature per line\n")

# load crypto library (default is pybitcointools, -ofek_ecdsa can override)
CRYPTO_LIBRARY=""
if len(sys.argv) > 2:
   print("Error: only one command line argument allowed!")
   show_usage()
   sys.exit(1)

elif len(sys.argv) == 2:
   if sys.argv[1] not in ('-ofek_ecdsa',):
      show_usage()
      sys.exit(1)

if len(sys.argv) == 2 and sys.argv[1] == '-ofek_ecdsa':
   try:
      import bit
      CRYPTO_LIBRARY='ofek_ecdsa'
   except ImportError:
      import bitcoin as pbt
      CRYPTO_LIBRARY='pybitcointools'

if CRYPTO_LIBRARY != 'ofek_ecdsa':
   try:
      import bitcoin as pbt
      CRYPTO_LIBRARY='pybitcointools'
   except ImportError:
      print("Error: unable to load fallback crypto library. File a bug report!")
      sys.exit(1)
else:
   import base64
   from ecdsa import VerifyingKey, SECP256k1
   from ecdsa.keys import BadSignatureError
   from bit.utils import hex_to_bytes
   from bit.format import verify_sig as ofek_verify_sig
   from bit.format import get_version as ofek_get_version
   from alai import DoubleSha256, pad_message


# some wrapper functions now that we support -ofek_ecdsa
def wrap_is_address(some_string):
   if CRYPTO_LIBRARY == 'pybitcointools':
      return pbt.is_address(pubkeys[i])
   elif CRYPTO_LIBRARY == 'ofek_ecdsa':
      try:
         addr_net = ofek_get_version(some_string)
         return (True if addr_net == 'main' else False)
      except ValueError:
         return False

def wrap_ecdsa_verify_addr(msg, sig, addr):
   if CRYPTO_LIBRARY == 'pybitcointools':
      return pbt.ecdsa_verify_addr(msg, sig, addr)
   elif CRYPTO_LIBRARY == 'ofek_ecdsa':
      sig_bytes, msg_bytes, addr_bytes = (bytes(sig, 'utf8'),
                                          bytes(msg, 'utf8'),
                                          bytes(addr, 'utf8'))
      return ofek_verify_sig(sig_bytes, msg_bytes, addr_bytes)


def wrap_ecdsa_verify(msg, sig, uncompressed_pubkey):
   if CRYPTO_LIBRARY == 'pybitcointools':
      return pbt.ecdsa_verify(msg, sig, uncompressed_pubkey)
   elif CRYPTO_LIBRARY == 'ofek_ecdsa':
      pk = hex_to_bytes(uncompressed_pubkey)
      sig_bytes, msg_bytes = base64.b64decode(sig), pad_message(msg)
      vk = VerifyingKey.from_string(pk, curve=SECP256k1, 
                                    hashfunc=DoubleSha256)
      try:
         print( vk.verify(sig_bytes[1:], msg_bytes, hashfunc=DoubleSha256) )
         return True
      except BadSignatureError:
         return False
      except:
         raise


# read in the data files
with open('pubkeys.txt') as pubkeyfile:
   pubkeys = pubkeyfile.readlines()
   pubkeyfile.close()

num_pubkeys = len(pubkeys)
if num_pubkeys == 0:
   print("Error: number of pubkeys/addresses is zero - check pubkeys.txt!")
   sys.exit(1)

with open('messages.txt') as msgfile:
   messages = msgfile.readlines()
   msgfile.close()

num_messages = len(messages)
if num_messages != num_pubkeys:
   print("Error: number of messages is not equal to number of "
         "pubkeys/addresses! - check pubkeys.txt and messages.txt!")
   sys.exit(1)

with open('signatures.txt') as sigfile:
   signatures = sigfile.readlines()
   sigfile.close()

num_signatures = len(signatures)
if num_signatures != num_pubkeys:
   print("Error: number of signatures is not equal to number of messages!"
         "- check signatures.txt!")
   sys.exit(1)

# strip any newlines from the data read
pubkeys, signatures, messages = (list(map(str.strip, pubkeys)),
                                 list(map(str.strip, signatures)),
                                 list(map(str.strip, messages)))

# initialize counters
valid_sigs = 0
invalid_sigs = 0
invalid_tuples = []


# run through all messages and check the provided signatures
for i in range(len(messages)):
   if wrap_is_address(pubkeys[i]):
      print("address #%d: %s" % (1+i, pubkeys[i]))
   else:
      print("pubkey #%d: %s" % (1+i, pubkeys[i]))

   print("message #%d: %s" % (1+i, messages[i]))
   print("signature #%d: %s" % (1+i, signatures[i]))

   if wrap_is_address(pubkeys[i]):
      valid = wrap_ecdsa_verify_addr(messages[i], signatures[i], pubkeys[i])
   else:
      valid = wrap_ecdsa_verify(messages[i], signatures[i], pubkeys[i])
   if valid:
      print("VALID signature")
      valid_sigs += 1
   else:
      print("INVALID signature")
      invalid_sigs += 1
      invalid_tuples.append((1+i, pubkeys[i], messages[i], signatures[i]))
   i += 1


# display final results
if invalid_sigs == 0:
   print("All %d signatures were valid." % len(signatures))
else:
   print("%d invalid signature(s) found:" % invalid_sigs)
   print("\nInvalid items:\n(row : key/addr --- message --- signature)")
   for inv_tuple in invalid_tuples:
      (row, pubkey_or_addr, msg, sig) = inv_tuple
      print("row %d : %s --- %s --- %s" % (row, pubkey_or_addr, msg, sig))
